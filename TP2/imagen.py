#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cv2

# Leer la imagen en modo de escala de grises
img = cv2.imread('hoja.png', 0)

# Definir el valor del umbral
valor_umbral = 50

# Alto y ancho de la imagen
altura, anchura = img.shape


for y in range(altura):
    for x in range(anchura):
        # Comprobar si el valor del pixel es menor que el umbral
        if img[y, x] < valor_umbral:
            img[y, x] = 0  # Asignar color negro
        else:
            img[y, x] = 255  # Asignar color blanco

# Guardar imagen
cv2.imwrite('resultado.png', img)
