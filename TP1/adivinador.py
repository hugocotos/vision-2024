import random

def adivina(intentos):
    numero_secreto = random.randint(0, 100)

    
    for i in range(intentos):
        try:
            entrada = int(input("Intenta adivinar el número secreto (entre 0 y 100): "))
        except ValueError:
            print("Por favor, introduce un número válido.")
            continue
        
        if entrada < 0 or entrada > 100:
            print("El número debe estar entre 0 y 100.")
        elif entrada == numero_secreto:
            print(f"¡Correcto! Adivinaste el número en {i + 1} intentos.")
            return True
        elif entrada < numero_secreto:
            print("Muy bajo. Intenta con un número más alto.")
        else:
            print("Muy alto. Intenta con un número más bajo.")
    
    print("Se han acabado los intentos. El número secreto era", numero_secreto)
    return False

def main():
    intentos_permitidos = 5
    print("Bienvenido al juego de adivinar el número.")
    print(f"Tienes {intentos_permitidos} intentos.")
    adivina(intentos_permitidos)

if __name__ == "__main__":
    main()
